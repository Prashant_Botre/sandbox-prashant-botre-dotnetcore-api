﻿using PBSAWebAPI.Models;
using System;
using System.Collections.Generic;

namespace PBSAWebAPI.Services
{
    public class AuthorService : IAuthorService
    {
        public List<Author> GetAuthors()
        {
            List<Author> authors = new List<Author>();
            authors.Add(new Author("Mahesh Chand", 35, "A Prorammer's Guide to ADO.NET", true, new DateTime(2003, 7, 10)));
            authors.Add(new Author("Neel Beniwal", 18, "Graphics Development with C#", false, new DateTime(2010, 2, 22)));
            authors.Add(new Author("Praveen Kumar", 28, "Mastering WCF", true, new DateTime(2012, 01, 01)));
            authors.Add(new Author("Mahesh Chand", 35, "Graphics Programming with GDI+", true, new DateTime(2008, 01, 20)));
            authors.Add(new Author("Raj Kumar", 30, "Building Creative Systems", false, new DateTime(2011, 6, 3)));

            return  authors;
        }
    }
}
