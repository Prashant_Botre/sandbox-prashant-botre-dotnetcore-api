﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PBSAWebAPI.Models;
using PBSAWebAPI.Services;
using System.Collections.Generic;

namespace PBSAWebAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly ILogger<AuthorController> _logger;

        private IAuthorService AuthorService { get; set; }
        public AuthorController(ILogger<AuthorController> logger, IAuthorService authorService)
        {
            _logger = logger;
            this.AuthorService = authorService;
        }

        [HttpGet]
        public IEnumerable<Author> Get()
        {
            List<Author> authors =  AuthorService.GetAuthors();

            return authors;
        }
    }
}
