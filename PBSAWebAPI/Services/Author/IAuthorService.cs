﻿using PBSAWebAPI.Models;
using System.Collections.Generic;

namespace PBSAWebAPI.Services
{
    public interface IAuthorService
    {
        List<Author> GetAuthors();
    }
}
